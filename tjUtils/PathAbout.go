package tjUtils

import (
	"errors"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"strings"
)

// GetMainAbsPath 得到编译后二进制文件所在 绝对路径
func GetMainAbsPath() (string, error) {
	exePath, err := os.Executable()
	if err != nil {
		return "", err
	}
	res, evalErr := filepath.EvalSymlinks(filepath.Dir(exePath))
	return res, evalErr
}

// PathExists 路径/文件 是否存在
func PathExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil { //文件或者目录存在
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

func MakeDir(dir string) error {
	exists, existErr := PathExists(dir)
	if existErr != nil {
		return existErr
	}

	if !exists {
		if err := os.MkdirAll(dir, 0777); err != nil { //os.ModePerm
			return err
		}
	}
	return nil
}

// WalkDir 递归遍历,返回 dir 开始的绝对路径, suffix 过滤指定后缀文件,为空则不过滤任何文件.
func WalkDir(dir, suffix string) (files []string, err error) {
	files = []string{}

	err = filepath.Walk(dir, func(fname string, fi os.FileInfo, err error) error {
		if fi.IsDir() {
			//忽略目录
			return nil
		}

		if len(suffix) == 0 || strings.HasSuffix(strings.ToLower(fi.Name()), suffix) {
			//文件后缀匹配
			files = append(files, fname)
		}
		return nil
	})

	return files, err
}

// ListDir 遍历路径,返回文件名,不进入下层路径,返回 dir 开始的绝对路径
//	suffix 过滤指定后缀文件,为空则不过滤任何文件.
func ListDir(dir, suffix string) (files []string, err error) {
	files = []string{}

	_dir, err := ioutil.ReadDir(dir)
	if err != nil {
		return nil, err
	}

	suffix = strings.ToLower(suffix) //匹配后缀

	for _, _file := range _dir {
		if _file.IsDir() {
			continue //忽略目录
		}
		if len(suffix) == 0 || strings.HasSuffix(strings.ToLower(_file.Name()), suffix) {
			//文件后缀匹配
			files = append(files, path.Join(dir, _file.Name()))
		}
	}

	return files, nil
}

// CopyDir 类似 unix cp 命令, 会覆盖原文件.
func CopyDir(srcPath, desPath string) error {
	//检查目录是否正确
	if srcInfo, err := os.Stat(srcPath); err != nil {
		return err
	} else {
		if !srcInfo.IsDir() {
			return errors.New("源路径不是一个正确的目录！")
		}
	}

	// 如果目标路径不存在, 则创建
	exists, pathExistErr := PathExists(desPath)
	if pathExistErr != nil {
		return pathExistErr
	}
	if !exists {
		if err := MakeDir(desPath); err != nil {
			return err
		}
	}

	// 再次检查 目标路径,无论是否发生创建
	if desInfo, err := os.Stat(desPath); err != nil {
		return err
	} else {
		if !desInfo.IsDir() {
			return errors.New("目标路径不是一个正确的目录！")
		}
	}

	if strings.TrimSpace(srcPath) == strings.TrimSpace(desPath) {
		return errors.New("源路径与目标路径不能相同！")
	}

	err := filepath.Walk(srcPath, func(path string, f os.FileInfo, err error) error {
		if f == nil {
			return err
		}

		//复制目录是将源目录中的子目录复制到目标路径中，不包含源目录本身
		if path == srcPath {
			return nil
		}

		//生成新路径
		destNewPath := strings.Replace(path, srcPath, desPath, -1)

		if !f.IsDir() {
			CopyFile(path, destNewPath)
		} else {
			exists, existErr := PathExists(destNewPath)
			if existErr != nil {
				return existErr
			}

			if !exists {
				return MakeDir(destNewPath)
			}
		}

		return nil
	})

	return err
}
