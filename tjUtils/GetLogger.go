package tjUtils

import (
	"fmt"
	nested "github.com/antonfisher/nested-logrus-formatter"
	"github.com/gggwvg/logrotate"
	"github.com/sirupsen/logrus"
	"os"
	"runtime"
	"strings"
)

func customerCaller(a *runtime.Frame) string {

	fileName := ""
	fileSplit := strings.Split(a.File, "/")
	if len(fileSplit) > 0 {
		fileName = fileSplit[len(fileSplit)-1]
	} else {
		fileName = a.File
	}

	funcName := ""
	funcSplit := strings.Split(a.Function, "/")
	if len(funcSplit) > 0 {
		funcName = funcSplit[len(funcSplit)-1]
	} else {
		funcName = a.Function
	}

	return fmt.Sprintf("[%s:%d][%s]",
		fileName,
		a.Line,
		funcName,
	)
}

func GetLogger(logFile, singleFileSize string, fileNum int, level string) *logrus.Logger {
	// 创建 日志 路径
	tmpSplit := strings.Split(logFile, "/")
	if len(tmpSplit) > 1 {
		logPath := strings.Join(tmpSplit[:len(tmpSplit)-1], "/")
		exists, _ := PathExists(logPath)
		if !exists {
			os.MkdirAll(logPath, os.ModePerm)
		}
	}

	opts := []logrotate.Option{
		logrotate.File(logFile),
		logrotate.MaxArchives(fileNum),
		logrotate.RotateSize(singleFileSize),
	}
	logger, err := logrotate.NewLogger(opts...)
	if err != nil {
		fmt.Println(err.Error())
		return nil
	}

	l := logrus.New()

	l.SetOutput(logger)

	// set level
	level = strings.ToLower(level)
	switch level {
	case "error":
		l.SetLevel(logrus.ErrorLevel)
	case "warn":
		l.SetLevel(logrus.WarnLevel)
	case "info":
		l.SetLevel(logrus.InfoLevel)
	case "debug":
		l.SetLevel(logrus.DebugLevel)
	case "trace":
		l.SetLevel(logrus.TraceLevel)
	default:
		l.SetLevel(logrus.InfoLevel)
	}

	l.SetReportCaller(true)

	l.SetFormatter(&nested.Formatter{
		HideKeys:              true,
		NoColors:              true,
		NoFieldsSpace:         true,
		TimestampFormat:       "[2006-01-02 15:04:05]",
		CallerFirst:           true,
		FieldsOrder:           []string{"name", "age"},
		CustomCallerFormatter: customerCaller,
	})

	return l
}
