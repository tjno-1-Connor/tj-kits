package tjUtils

import (
	"math"
	"time"
)

type TimeKit struct {
	Day     int64   // 相差的天数, 只取 天数
	Hour    int64   // ...
	Minute  int64   // ...
	Second  int64   // ...
	THour   float64 // 相差的 总小时数
	TMinute float64 // 相差的 总分钟数
	TSecond float64 // 相差的 总秒数
}

// NowTT 当前时间戳(秒) int64
func NowTT() int64 {
	timeUnix := time.Now().Unix()
	return timeUnix
}

// NowTTT 当前时间戳(毫秒) int64
func NowTTT() int64 {
	timeUnixNano := time.Now().UnixNano()
	return timeUnixNano
}

// NowTS 当前时间 "2006-01-02 15:04:05"
func NowTS() string {
	timeUnix := time.Now().Unix()
	formatTimeStr := time.Unix(timeUnix, 0).Format("2006-01-02 15:04:05")
	return formatTimeStr
}

// TTToStr 时间戳 转 字符串
func TTToStr(timeUnix int64) string {
	formatTimeStr := time.Unix(timeUnix, 0).Format("2006-01-02 15:04:05")
	return formatTimeStr
}

// StrToTT 字符串转 时间戳
func StrToTT(timeStr string) int64 {
	formatTime, _ := time.Parse("2006-01-02 15:04:05", timeStr)
	return formatTime.Unix()

}

// GetTimeDiffStr 两个时间字符串 计算时差
func GetTimeDiffStr(a, b string) *TimeKit {
	t1 := StrToTT(a)
	t2 := StrToTT(b)

	res := GetTimeDiffTT(t1, t2)
	return res
}

// GetTimeDiffTT 两个时间戳符串 计算时差
func GetTimeDiffTT(a, b int64) *TimeKit {
	timeKit := &TimeKit{}

	// 时间戳 (int64) 转 time
	time1 := time.Unix(a, 0)
	time2 := time.Unix(b, 0)

	subM := time1.Sub(time2)

	// 两个时间 相差的 总数的, 不同单位的表示
	totalHour := subM.Hours()
	totalMinute := subM.Minutes()
	totalSecond := subM.Seconds()

	// step 1: 获取 正负数 单位
	if (totalHour + totalMinute + totalSecond) == 0.0 {
		timeKit.Day = 0
		timeKit.Hour = 0
		timeKit.Minute = 0
		timeKit.Second = 0
		timeKit.THour = totalHour
		timeKit.TMinute = totalMinute
		timeKit.TSecond = totalSecond
		return timeKit
	}

	var symbol int64 = 1
	if (totalHour + totalMinute + totalSecond) < 0.0 {
		symbol = -1
	}

	// step 2: 转换为 绝对值
	tHour := math.Abs(totalHour)
	tMinute := math.Abs(totalMinute)
	tSecond := math.Abs(totalSecond)

	// step 3: 计算 Day, Hour, Min, Sec
	tHourInt := int64(tHour)     // 整 小时 数
	tMinuteInt := int64(tMinute) // 整 分钟 数
	tSecondInt := int64(tSecond)

	// 秒数 =  整秒 - (整分 * 60)
	tmpSecond := tSecondInt - (tMinuteInt * 60)

	// 分钟数 = 整分 - (整小时 * 60)
	tmpMinute := tMinuteInt - (tHourInt * 60)

	// 整天 = 整小时 % 24
	tmpDay := int64(tHour / 24.0)

	// 整小时 = 整小时 - (整天 * 24)
	tmpHour := tHourInt - (tmpDay * 24)

	timeKit.Day = tmpDay * symbol
	timeKit.Hour = tmpHour * symbol
	timeKit.Minute = tmpMinute * symbol
	timeKit.Second = tmpSecond * symbol
	timeKit.THour = totalHour
	timeKit.TMinute = totalMinute
	timeKit.TSecond = totalSecond

	return timeKit
}
