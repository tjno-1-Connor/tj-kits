package tjUtils

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strings"
)

// WholeFileToSlice 不适合读取大文 (>1mb)
func WholeFileToSlice(fileName string) ([]string, error) {
	fileHanle, err := os.OpenFile(fileName, os.O_RDONLY, 0666)
	if err != nil {
		return nil, err
	}

	defer fileHanle.Close()

	readBytes, err := ioutil.ReadAll(fileHanle)
	if err != nil {
		return nil, err
	}

	res := strings.Split(string(readBytes), "\n")

	return res, nil
}

// ReadFilePerLine 参考,不适合直接使用
func ReadFilePerLine(fileName string) error {
	fileHanle, err := os.OpenFile(fileName, os.O_RDONLY, 0666)
	if err != nil {
		return err
	}

	defer fileHanle.Close()

	reader := bufio.NewReader(fileHanle)

	var results []string
	// 按行处理txt
	for {
		line, _, err := reader.ReadLine()
		if err == io.EOF {
			break
		}
		results = append(results, string(line))
	}
	fmt.Printf("read result:%v\n", results)
	return nil
}

// CopyFile 使用io.Copy, 保持 文件权限
func CopyFile(src, des string) (written int64, err error) {
	srcFile, err := os.Open(src)
	if err != nil {
		return 0, err
	}
	defer srcFile.Close()

	//获取源文件的权限
	fi, _ := srcFile.Stat()
	perm := fi.Mode()

	//desFile, err := os.Create(des)  //无法复制源文件的所有权限
	desFile, err := os.OpenFile(des, os.O_RDWR|os.O_CREATE|os.O_TRUNC, perm) //复制源文件的所有权限
	if err != nil {
		return 0, err
	}
	defer desFile.Close()

	return io.Copy(desFile, srcFile)
}
