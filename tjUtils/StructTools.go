package tjUtils

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"path"
	"strings"
)

// SimpleStructToFile 只适用于单层 json, 确保路径存在,覆盖写入.
func SimpleStructToFile(s any, pathToFile string) error {
	filePath := path.Dir(pathToFile)

	exists, err := PathExists(filePath)
	if err != nil {
		return err
	}

	if !exists {
		return errors.New("path not exists")
	}

	marshalBytes, marshalErr := json.Marshal(s)
	if marshalErr != nil {
		return marshalErr
	}

	str := string(marshalBytes)
	triml := strings.Trim(str, "{")
	trimr := strings.Trim(triml, "}")
	trimSplit := strings.Split(trimr, ",")
	operaFile, openFileErr := os.OpenFile(pathToFile, os.O_WRONLY|os.O_CREATE, 0666)
	if openFileErr != nil {
		return openFileErr
	}
	defer operaFile.Close()

	operaWriter := bufio.NewWriter(operaFile)

	operaWriter.WriteString(fmt.Sprintf("%s\n", "{"))

	for _, value := range trimSplit {
		operaWriter.WriteString(fmt.Sprintf("\t%s,\n", value))
	}

	operaWriter.WriteString(fmt.Sprintf("%s\n", "}"))
	operaWriter.Flush()

	return nil
}
