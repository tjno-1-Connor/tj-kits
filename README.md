# tj-kits

### Usage

```
import "gitee.com/tjno-1-Connor/tj-kits/tjUtils"

go get -u "gitee.com/tjno-1-Connor/tj-kits/tjUtils"
go mod tidy
```

GetLogger("logs/abc.log", "5mb", 10, "debug")
* path 'logs/' will create auto.
* level:
  * "" | "xxx": defaults is info
  * error
  * warn
  * info
  * debug
  * trace
