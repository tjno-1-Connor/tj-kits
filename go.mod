module gitee.com/tjno-1-Connor/tj-kits

go 1.18

require (
	github.com/antonfisher/nested-logrus-formatter v1.3.1
	github.com/gggwvg/logrotate v0.0.0-20200322124011-2c1c889f862b
	github.com/sirupsen/logrus v1.9.0
)

require (
	github.com/gggwvg/crontab v1.0.0 // indirect
	github.com/kr/fs v0.1.0 // indirect
	github.com/pkg/sftp v1.13.5 // indirect
	golang.org/x/crypto v0.0.0-20220919173607-35f4265a4bc0 // indirect
	golang.org/x/sys v0.0.0-20220919091848-fb04ddd9f9c8 // indirect
)
